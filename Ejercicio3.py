#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import random


def notas_matriz(notas):
    print("esta es la matriz generada de notas:")
    x = 0
    y = 0
    alumno = 1
    xmax = 4
    ymax = 31
    """
    Se mostrara al usuario las notas que se obtienen, para
    comprobar que los resultados estan correctos
    """
    for y in range(y, ymax):
        print("Alumno numero ", alumno)
        for x in range(x, xmax):
            print("[", end='')
            print(notas[x], end='')
            print("]", end='')
        print("\n")
        x = xmax
        xmax = xmax + 4
        alumno = alumno + 1
    print("\n")
    return


def notas_finales(notas):
    alumno = 1
    x = 0
    y = 0
    xmax = 4
    ymax = 31
    """
    para tomar las notas finales, se inventara un limite entre el 0 hasta el
    4, ya que la lista es solo una linea llena de numeros, estos valores
    seran sumados, y al final divididos, en el caso que estos sean menor
    a un 4.0 sin incluirlo, seran reprobados, caso contrario, habran
    aprobado su clase de informatica, y asi ira con cada alumno
    """
    for y in range(y, ymax):
        print("Alumno numero: ", alumno)
        notafinal = 0
        for x in range(x, xmax):
            notafinal = notas[x] + notafinal
        notafinal = notafinal / 4
        print("Nota: ", round(notafinal, 2))
        if notafinal < 4.0:
            print("Estado: Reprobado")
        elif notafinal >= 4.0:
            print("Estado: Aprobado")
        alumno = alumno + 1
        x = xmax
        xmax = xmax + 4
        print("\n")
    return


print("Notas de programacion 1")
print("\n")
x = 4
y = 31
tam = x * y
notas = []
"""
Se crearan las notas por medio de un random con float, pero estos
solo llevaran una decima al hacer round y el ', 1'
"""
for x in range(0, tam):
    notas.append(round(random.uniform(1, 7), 1))
notas_matriz(notas)
notas_finales(notas)
