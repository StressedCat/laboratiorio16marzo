#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import random


def matriz_print(matriz):
    print("esta es la matriz generada:")
    x = 0
    y = 0
    xmax = 12
    ymax = 15
    # Aca se imprimira la matriz para que el usuario vea como quedo
    # Respetando cada espacio de esta
    for y in range(y, ymax):
        for x in range(x, xmax):
            print("[", end='')
            print(matriz[x], end='')
            print("]", end='')
        print("\n")
        x = xmax
        xmax = xmax + 12
    print("\n")
    return


def matriz_menormayor(matriz, tam):
    print("Buscador de menores y mayores en la matriz")
    nummen = 10
    posmen = 0
    nummay = -10
    posmay = 0
    # Se buscara cual es el mayor y cual es el menor
    """
    Para obtener el resultado mas exacto, se colocara preterminadamente
    en el valor minimo el mayor posible, mientras que en el menor, se colocara
    el mayor valor posible
    """
    for x in range(0, tam):
        if matriz[x] > nummay:
            nummay = matriz[x]
            posmay = x
        if matriz[x] < nummen:
            nummen = matriz[x]
            posmen = x
    print("El primer numero mayor encontrado fue: ", nummay)
    print("La posicion de este numero en la matriz es :", posmay)
    print("\n")
    print("El primer numero menor encontrado fue: ", nummen)
    print("La posicion de este numero en la matriz es: ", posmen)
    print("\n")
    return


def matriz_SumaQuintoElemento(matriz):
    x = 0
    y = 0
    xmax = 12
    ymax = 15
    sumtotal = 0
    """
    Para obtener cada suma de la primera hasta la quinta columna
    se creara un limite para que tome los valores hasta tal punto
    """
    for y in range(y, ymax):
        for x in range(x, xmax):
            if x <= xmax - 8:
                sumtotal = matriz[x] + sumtotal
        x = xmax
        xmax = xmax + 12
    print("La suma total de la primera columna hasta la quinta es: ", sumtotal)
    print("\n")
    return


def matriz_NumerosNegativos(matriz):
    print("Numeros negativos desde la columna quinta a la nueve")
    x = 0
    y = 0
    xmax = 12
    ymax = 15
    negnum = 0
    """
    al igual que la anterior, se creara un valor entre y hasta, ya que no
    se deben contar los valores desde el primero hasta el cuarto y al
    mismo tiempo los valores del diez hasta el doce
    """
    for y in range(y, ymax):
        for x in range(x, xmax):
            if x >= xmax - 8 and x <= xmax - 4:
                if matriz[x] < 0:
                    negnum = negnum + 1
        x = xmax
        xmax = xmax + 12
    print("el total de numeros negativos es: ", negnum)
    print("\n")
    return


print("Matriz automatica de randoms de -10 a 10")
matriz = []
tam = 15 * 12
suma = 0
rep = 0
# Se agregaran valores entre -10 y 10 a la lista
# Esto sera hasta que rep sea igual al tam, siendo tam el tamaño de la matriz
while(rep != tam):
    rep = rep + 1
    matriz.append(random.randint(-10, 10))
matriz_print(matriz)
matriz_menormayor(matriz, tam)
matriz_SumaQuintoElemento(matriz)
matriz_NumerosNegativos(matriz)
