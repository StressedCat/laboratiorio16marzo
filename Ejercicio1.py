#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def Tupla_a_lista(tupla):
    # La tupla sera convertida a una lista por medio del comando siguiente
    lista = list(tupla)
    # Para saber el tamaño de la lista, se usara tam
    tam = len(lista)
    # Existiran dos intentos, uno para añadir, otro para eliminar
    for y in range(0, 2):
        for x in range(0, tam):
            # En el caso que sea el primer intento, se agregaran valores
            if y == 0:
                # Todo valor de la lista sera agregado al final, copiandola
                # Todos excepto el ultimo
                if x != tam - 1:
                    lista.append(lista[x])
            # En el caso que sea el segundo intento, se eliminaran valores
            if y == 1:
                # Todo primer valor de la lista sera eliminado
                # Todo menos el ultimo
                if x != tam - 1:
                    lista.remove(lista[0])
    print("La lista modificada se expresara de la siguiente forma: ")
    print(lista)
    return


print("Se usara la siguiente tupla para correr todo numero a la derecha")
# Se creara una tupla predeterminada
tupla = (1, 3, 5, 2, 9, 13)
print(tupla)
Tupla_a_lista(tupla)
